package facci.leonel.anchundia.conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText centi,faren;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.centi=(EditText) findViewById(R.id.txtCentigrados);
        this.faren=(EditText) findViewById(R.id.txtFahrenheit);

//        eventos

        this.centi.setOnEditorActionListener(new TextView.OnEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//logica
                float fa;
                fa=(1.8f)*Float.parseFloat(centi.getText().toString())+32;
                faren.setText(""+fa);
                return false;
            }
        });

        this.faren.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//logica
                float ce;
                ce=(Float.parseFloat(faren.getText().toString())-32)/(1.8f);
                centi.setText(""+ce);

                return false;
            }
        });
    }
}